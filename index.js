const express = require("express")
const app = express()
const swaggerUI = require("swagger-ui-express")
const swaggerJSON = require("./swagger.json")

app.get("/api-docs", (req, res) => {
    res.json(swaggerJSON)
})

app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON))

app.listen(8080)
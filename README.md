## Description
Projek ini adalah projek yang dimana membuat sebuah REST API yang dapat digunakan untuk melakukan manajemen data mobil dengan fitur authentication.Projek ini sedang dalam tahap pengembangan, baru open API nya saja yang baru dibuat.


## How to run
guide ...

```bash
how to run
```

## Endpoints
list endpoints ...

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
├── models
│   └── index.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
├── .gitignore
├── README.md
├── swagger.json
├── swagger.yaml
├── index.js
├── package.json
```


